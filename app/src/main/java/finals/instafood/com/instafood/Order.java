package finals.instafood.com.instafood;

/**
 * Created by Shanyl Jimenez on 10/16/2015.
 */
public class Order {
    private int OrderID;
    private int MenuID;
    private String MenuName;
    private int Quantity;

    public Order() {
    }

    public Order(int orderID, int menuID, String menuName, int quantity) {
        OrderID = orderID;
        MenuID = menuID;
        MenuName = menuName;
        Quantity = quantity;
    }

    public int getOrderID() {
        return OrderID;
    }

    public void setOrderID(int orderID) {
        OrderID = orderID;
    }

    public int getMenuID() {
        return MenuID;
    }

    public void setMenuID(int menuID) {
        MenuID = menuID;
    }

    public String getMenuName() {
        return MenuName;
    }

    public void setMenuName(String menuName) {
        MenuName = menuName;
    }

    public int getQuantity() {
        return Quantity;
    }

    public void setQuantity(int quantity) {
        Quantity = quantity;
    }
}
