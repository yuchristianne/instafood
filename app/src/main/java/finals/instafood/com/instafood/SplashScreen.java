package finals.instafood.com.instafood;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;
import android.widget.Toolbar;

import finals.instafood.com.instafood.DataBaseSQLite;

import android.os.Handler;

import java.util.ArrayList;

public class SplashScreen extends Activity {
    private static int SPLASH_TIME_OUT = 3000;
    DataBaseSQLite dtbsql;



    @Override
    public PendingIntent createPendingResult(int requestCode, Intent data, int flags) {
        return super.createPendingResult(requestCode, data, flags);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

       dtbsql=new DataBaseSQLite(this);


        boolean isinserted1=dtbsql.InsertIntoMenuDb(1,"insta burger",159,"Classic burger with toasted sesame bun");
        boolean isinserted2=dtbsql.InsertIntoMenuDb(2,"cheeseburger",179,"Classic burger with lettuce, tomato and thick layer of cheese.");
        boolean isinserted3=dtbsql.InsertIntoMenuDb(3,"BBQ bacon burger",179,"Smoked mozzarella, bbq bacon mayo");
        boolean isinserted4=dtbsql.InsertIntoMenuDb(4,"American chicken burger",179,"Chicken Swiss, lettuce, tomato, special sauce");
        boolean isinserted5=dtbsql.InsertIntoMenuDb(5,"fried chicken",159,"1 pc. Juicy friend chicken, steaming rice with crispy fries");
        boolean isinserted6=dtbsql.InsertIntoMenuDb(6,"double fried chicken",179,"2 pcs. Juicy friend chicken, steaming rice with crispy fries");
        boolean isinserted7=dtbsql.InsertIntoMenuDb(7,"fried chicken with spaghetti",159,"1 pc. Juicy friend chicken, steaming rice with crispy fries");
        boolean isinserted8=dtbsql.InsertIntoMenuDb(8,"cheesy baked lasagna",189,"Layered Lasagna noodles, italian sausage and beef ricotta cheese, tomato sauce, baked mozzarella and romano cheese");
        boolean isinserted9=dtbsql.InsertIntoMenuDb(9,"fruit juice",66,"fruit juice make from real fresh fruits");
        boolean isinserted0=dtbsql.InsertIntoMenuDb(10,"Diet Coke",58,"Chilled coke perfect for the hot weather");
        boolean isinserted10=dtbsql.InsertIntoMenuDb(11,"millionaires coffee",79,"Office liquor and hazel nut liquor");
        boolean isinserted11=dtbsql.InsertIntoMenuDb(12,"winter melon milk tea",79, "Fruit fresh from taiwan turned into tea and juice mix with milk");

if(isinserted1==true && isinserted2==true && isinserted3==true && isinserted4==true && isinserted5==true && isinserted6==true && isinserted7==true && isinserted8==true &&isinserted9==true && isinserted0==true && isinserted10==true && isinserted11==true) {
Toast.makeText(this,"Data inserted",Toast.LENGTH_LONG).show();
}
        else {
    Toast.makeText(this, "Data not inserted", Toast.LENGTH_LONG).show();
}



        new Handler().postDelayed(new Runnable() {


            @Override
            public void run() {
                Intent i = new Intent(SplashScreen.this, CustomerLog.class);
                startActivity(i);

                finish();
            }
        }, SPLASH_TIME_OUT);

    }


}
