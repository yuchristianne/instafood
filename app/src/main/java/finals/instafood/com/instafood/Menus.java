package finals.instafood.com.instafood;

/**
 * Created by Shanyl Jimenez on 10/16/2015.
 */
public class Menus {
    private int MenuID;
    private String MenuName;
    private int price;
    private String MenuDescription;


    public Menus() {
    }

    public Menus(int menuID, String menuName, int price, String menuDescription) {
        MenuID = menuID;
        MenuName = menuName;
        this.price = price;
        MenuDescription = menuDescription;
    }

    public int getMenuID() {
        return MenuID;
    }

    public void setMenuID(int menuID) {
        MenuID = menuID;
    }

    public String getMenuName() {
        return MenuName;
    }

    public void setMenuName(String menuName) {
        MenuName = menuName;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getMenuDescription() {
        return MenuDescription;
    }

    public void setMenuDescription(String menuDescription) {
        MenuDescription = menuDescription;
    }
}
