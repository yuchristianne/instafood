package finals.instafood.com.instafood;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by User on 10/15/2015.
 */
public class DataBaseSQLite extends SQLiteOpenHelper {

    //Database Name
    public static final String DataBase_Name ="Instafood.db";

    //Menus Table
    public static final String Table_Menu="MENU";
    public static final String MenuID="MenuID";
    public static final String columnmenuname="MenuName";
    public static final String columnprize="Prize";
    public static final String columndescription="Description";

    //Order Table
    public static final String Table_Order="ORDERS";
    public static final String columnOrderId="OrderID";
    public static final String columnOrderMenuId="OrderMenuID";
    public static final String columnQuantity="Quantity";


    //Db version
    public static final int database_version = 1;




    public DataBaseSQLite(Context context) {
        super(context, DataBase_Name, null, database_version);
        Log.d("DataBase SQLite", "DataBase Created");
    }


    public void onCreate(SQLiteDatabase dbs) {

        dbs.execSQL("Create table " + Table_Menu + " (MenuID INTEGER PRIMARY KEY AUTOINCREMENT,MenuName TEXT,Prize INTEGER,Description TEXT);");
        dbs.execSQL("Create table " + Table_Order + " (OrderID INTEGER PRIMARY KEY AUTOINCREMENT,OrderMenuID INTEGER,MenuName TEXT,Quantity INTEGER);");
    }


    public void onUpgrade(SQLiteDatabase dbs, int oldVersion, int newVersion) {
        dbs.execSQL("DROP TABLE IF EXISTS "+Table_Menu);
        dbs.execSQL("DROP TABLE IF EXISTS "+Table_Order);
        onCreate(dbs);

    }



    //////////////////////MENU_TABLE///////////////////////

    public boolean InsertIntoMenuDb(int menuid,String menu_name, int prize, String desc) {

        SQLiteDatabase sq = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(MenuID, menuid);
        cv.put(columnmenuname, menu_name);
        cv.put(columnprize, prize);
        cv.put(columndescription, desc);
        long sqinsert= sq.insert(Table_Menu, null, cv);

        if(sqinsert== -1) {
            return false;
        }else {
            return true;
        }
    }


    public ArrayList<Menus> FetchMenuData()
    {
        ArrayList<Menus> MList= new ArrayList<Menus>();
        SQLiteDatabase sq = this.getWritableDatabase();
        Cursor cr=sq.rawQuery("select * from " + Table_Menu, null);

        while(cr.moveToNext())
        {
            MList.add(new Menus(cr.getInt(0),cr.getString(1),cr.getInt(2),cr.getString(3)));
        }

        return MList;
    }

    //////////////////////ORDERTABLE//////////////////////////

    public boolean InsertIntoOrderDb(int orderid,int menuid,String menuName,int quan) {

        SQLiteDatabase sq = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(columnOrderId, orderid);
        cv.put(columnOrderMenuId,menuid);
        cv.put(columnmenuname,menuName);
        cv.put(columnQuantity,quan);
        sq.insert(Table_Order, null, cv);

        long sqinserts= sq.insert(Table_Order, null, cv);

        if(sqinserts== -1) {
            return false;
        }else {
            return true;
        }
    }



    public ArrayList<Order> FetchOrderData()
    {
        ArrayList<Order> OList= new ArrayList<Order>();
        SQLiteDatabase sq = this.getWritableDatabase();
        Cursor cr=sq.rawQuery("select * from "+Table_Order,null);

        while(cr.moveToNext())
        {
            OList.add(new Order(cr.getInt(0),cr.getInt(1),cr.getString(2),cr.getInt(3)));
        }

        return OList;


    }

    public boolean UpdateOrder(int orderid,int menuid,String menuName,int quan)
    {
        SQLiteDatabase sq = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(columnOrderId,orderid );
        cv.put(columnOrderMenuId,menuid);
        cv.put(columnmenuname, menuName);
        cv.put(columnQuantity, quan);

        sq.update(Table_Order,cv,"OrderID = ?",new String[]{ MenuID });
        return true;
    }


    public Integer DeleteOrder(String id)
    {
        SQLiteDatabase sq = this.getWritableDatabase();
        return sq.delete(Table_Order,"MenuID = ?",new String[]{id});

    }

}
