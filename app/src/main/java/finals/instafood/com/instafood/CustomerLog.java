package finals.instafood.com.instafood;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;


public class CustomerLog extends AppCompatActivity {

    EditText txtName, txtContactNum;
    Button btnLogin;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.customerlog);
        txtName = (EditText) findViewById(R.id.name);
        txtContactNum = (EditText) findViewById(R.id.contact);
        btnLogin = (Button) findViewById(R.id.button);



    }


    public void next (View v) {

        String name = txtName.getText().toString();
        String contactNum = txtContactNum.getText().toString();

        if (name.equals("")) {
            txtName.setError("Please enter a valid name.");
        }
        if (contactNum.equals("")) {
            txtContactNum.setError("Please enter a valid number");
        } else {

            Intent i = new Intent(this, MenuActivity.class);


            i.putExtra("key_name", name);
            i.putExtra("key_num", contactNum);
            startActivity(i);
        }
    }
}
