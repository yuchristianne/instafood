package finals.instafood.com.instafood;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Random;

public class MenuActivity extends AppCompatActivity implements TextToSpeech.OnInitListener {


    private FloatingActionButton btnSpeak;
    private String Name;
    private String Contact;
    private String content;
    private int quantity;
    private String orderCode;


    private ArrayList<Menus> MList= new ArrayList<Menus>();
    private ArrayList<Order> Olist= new ArrayList<Order>();



    private static final int REQ_CODE_SPEECH_INPUT = 10000;
    private TextToSpeech mTts;

    DataBaseSQLite dtbsql;
    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ///// INITIALIZE /////////////////////////////////////////////

        dtbsql= new DataBaseSQLite(this);
        Intent i= getIntent();
        Name= i.getStringExtra("key_name");
        Contact=i.getStringExtra("key_num");
        btnSpeak=(FloatingActionButton)findViewById(R.id.fabi);
        mTts= new TextToSpeech(this, this);
        btnSpeak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                promptSpeechInput();
            }});

        MList=dtbsql.FetchMenuData();


        //////////////////////////////////////////////////////////////


    }

    public void TakeOrder(String content)
    {
        MList=dtbsql.FetchMenuData();
        int a=0,flag=0,index=0;
        content.toLowerCase();
        String[] words = content.split(" ");


        if(content.equals("that would be all"))
        {
            Intent n = new Intent(getApplicationContext(), Account.class);
            n.putExtra("Name",Name);
            n.putExtra("Contact",Contact);
            startActivity(n);

        }
        else if(content.equals("cancel"))
        {
            speak("What order would you want to cancel?");
            while (mTts.isSpeaking()){}
            // promptSpeechInput();
        }
        else {
            try {
                quantity=Integer.parseInt(words[a]);
            }
            catch (Exception e)
            {
                if(words[0].equals("one"))
                {   quantity=1; }
                else if(words[0].equals("two"))
                {   quantity=2; }
                else if(words[0].equals("three"))
                {   quantity=3; }
                else if(words[0].equals("four"))
                {   quantity=4; }
                else if(words[0].equals("five"))
                {   quantity=5; }
                else {
                    speak("Sorry but your order quantity is invalid. Can you please say it again?");
                    while (mTts.isSpeaking()) {
                    }
                    promptSpeechInput();
                }
            }
            for(a=1;a<words.length;a++)
            {
                if(a==1 && words.length>a)
                {
                    orderCode=words[1]+" ";
                }else if(a==words.length-1)
                {
                    orderCode+=words[a];
                }
                else
                {orderCode+=words[a]+" ";}
            }
            for (index=0;index<MList.size();index++)
            {
                if(MList.get(index).getMenuName().equals(orderCode))
                {flag++;

                    break;}
            }

            if(flag>0)
            {


                Random rn = new Random();

                int flagRan=0,num=0,pn;

                if(Olist.size()==0)
                {
                    num=rn.nextInt(20) + 1;
                }
                else
                {   while(flagRan==0)
                {
                    num= rn.nextInt(20) + 1;
                    for (int i = 0; i < Olist.size(); i++) {
                        pn=Olist.get(i).getOrderID();
                        if(num==pn)
                        { flagRan=0;
                            break;}
                        else
                            flagRan++;

                    }}
                }


                dtbsql.InsertIntoOrderDb(num,MList.get(index).getMenuID(),MList.get(index).getMenuName(),quantity);
                Olist=dtbsql.FetchOrderData();
                Toast.makeText(getApplicationContext(),"Order inserted " + MList.get(index).getMenuName(), Toast.LENGTH_LONG).show();
            }


            quantity=0;
            orderCode=null;


        }
    }


    private void speak(String text) {
        if (TextUtils.isEmpty(text)) {
            return;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mTts.speak(text, TextToSpeech.QUEUE_FLUSH, null, null);
        } else {
            mTts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
        }
    }


    private void promptSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent
                .LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Say quantity then the MENU NAME");

        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);

        } catch (Exception ex) {
            speak("We're sorry but please some again?");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == REQ_CODE_SPEECH_INPUT) {
            ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            if (result != null && !result.isEmpty())
            {
                content=result.get(0);
                content.toLowerCase();
                Toast.makeText(getApplicationContext(), content, Toast.LENGTH_LONG).show();

                TakeOrder(content);


            }

        }
    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void nextacc (View view){



        Intent in = new Intent(getApplicationContext(), Account.class);
        startActivity(in);
    }


    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {
            int result = mTts.setLanguage(Locale.ENGLISH);

            if (result == TextToSpeech.LANG_MISSING_DATA ||
                    result == TextToSpeech.LANG_NOT_SUPPORTED) {
            } else {
                speak("Hi "+Name+"! How are you?. Welcome to Instafood. May I take your order?");

            }
        } else {
//            Snackbar.make(mFab, R.string.cannot_speak, Snackbar.LENGTH_LONG).show();
//
//            mFab.setEnabled(false);
//            mEtInput.setEnabled(false);
        }
    }

    private boolean isNumber(String word)
    {
        boolean isNumber = false;
        try
        {
            Integer.parseInt(word);
            isNumber = true;
        } catch (NumberFormatException e)
        {
            isNumber = false;
        }
        return isNumber;
    }

}

