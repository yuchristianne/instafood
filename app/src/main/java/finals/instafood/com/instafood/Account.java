package finals.instafood.com.instafood;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;

public class Account extends AppCompatActivity {

    DataBaseSQLite dtbsql;
    Button sendButton;
    private String Name;
    private String Contact;
    private String contents;

    TextView total;
    TextView content;

    private ArrayList<Menus> MList= new ArrayList<Menus>();
    private ArrayList<Order> Olist= new ArrayList<Order>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dtbsql=new DataBaseSQLite(this);
        setContentView(R.layout.activity_account);
        sendButton=(Button)findViewById(R.id.btnSend);
        total=(TextView)findViewById(R.id.totalprice);
        content=(TextView)findViewById(R.id.content);

        Olist=dtbsql.FetchOrderData();
        MList=dtbsql.FetchMenuData();
        Toast.makeText(getApplicationContext(), MList.get(0).getMenuName() +" and " + Olist.get(0).getOrderID(), Toast.LENGTH_LONG).show();

        int subSum=0,Mindex=0;
        int totals=0;
        for (int a=0;a<Olist.size();a++)
        {
            for (int b=0;b<MList.size();b++)
            {
                if(Olist.get(a).getMenuID()==MList.get(b).getMenuID())
                {   Mindex=b;
                    b=MList.size();}
            }
            if(a==0)
            {
                subSum=Olist.get(a).getQuantity()*MList.get(Mindex).getPrice();
                totals+=subSum;
                contents= Olist.get(a).getQuantity()+"\t"+MList.get(Mindex).getMenuName()+"\t"+subSum +"\n";
            }
            else {
                subSum = Olist.get(a).getQuantity() * MList.get(Mindex).getPrice();
                totals += subSum;
                contents += Olist.get(a).getQuantity() + "\t" + MList.get(Mindex).getMenuName() + "\t" + subSum + "\n";
            }
        }
        content.setText(contents+"\n"+"Total : Php"+totals+".00");
        total.setText("Php "+totals+".00");


    }

    public void send(View view){
        Intent i = new Intent(this, ThankYouActivity.class);
        Intent c=getIntent();
        Name=c.getStringExtra("Name");
        Contact=c.getStringExtra("Contact");
        i.putExtra("Name",Name);
        i.putExtra("Contact", Contact);


//        // add the phone number in the data
//        Uri uri = Uri.parse("smsto:" + Contact);
//        Intent smsSIntent = new Intent(Intent.ACTION_SENDTO, uri);
//        // add the message at the sms_body extra field
//        smsSIntent.putExtra("sms_body","Hi "+Name);
//        try{
//            startActivity(smsSIntent);
//        } catch (Exception ex) {
//            Toast.makeText(this, "Your sms has failed...",Toast.LENGTH_LONG).show();
//            ex.printStackTrace();
//        }

        Intent smsIntent = new Intent(android.content.Intent.ACTION_VIEW);
        smsIntent.setType("vnd.android-dir/mms-sms");
        smsIntent.putExtra("address", Contact);
        smsIntent.putExtra("sms_body","Hi "+Name+ "\n"+contents );
        startActivity(smsIntent);



        //startActivity(i);




    }



//        btnSend.setOnClickListener(new OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//
//                try {
//                    SmsManager smsManager = SmsManager.getDefault();
//                    smsManager.sendTextMessage(Contact, null, "Hi " + Name, null, null);
//                    Toast.makeText(getApplicationContext(), "SMS Sent!",
//                            Toast.LENGTH_LONG).show();
//                } catch (Exception e) {
//                    Toast.makeText(getApplicationContext(),
//                            "SMS faild, please try again later!",
//                            Toast.LENGTH_LONG).show();
//                    e.printStackTrace();
//                }
//            }
//        });
//
//
}
