package finals.instafood.com.instafood;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class Profile extends AppCompatActivity {

    private TextView mName;
    private TextView mNum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        mName = (TextView) findViewById(R.id.name);
        mNum = (TextView) findViewById(R.id.contact);
        setSupportActionBar(toolbar);



        Intent in = getIntent();
        String name = in.getStringExtra("key_name");
        String contactNum = in.getStringExtra("key_num");
        Toast.makeText(this, mName + ", " + mNum, Toast.LENGTH_LONG).show();

        mName.setText(name);
        mNum.setText(contactNum);

    }

}
